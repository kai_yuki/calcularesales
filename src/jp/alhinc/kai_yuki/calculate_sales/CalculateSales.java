package jp.alhinc.kai_yuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		Map<String, String> branchesNames = new HashMap<>();
		Map<String, Long> branchesSales = new HashMap<>();
		
		//エラー処理3 処理内容に記載のないエラーメッセージは「予期せぬエラーが発生しました」を出力する。
		//処理1 支店定義ファイル読み込み
		/*
		1-1 ファイルが存在しない場合は、エラーメッセージ「支店定義ファイルが存在しません」を表示し、処理を終了する。
		1-2 フォーマットが不正な場合は、エラーメッセージ「支店定義ファイルのフォーマットが不正です」を表示し、処理を終了する。
		*/
		try {
			//コマンドライン引数で指定されたディレクトリから支店定義ファイルを開く。
			File file = new File(args[0], "branch.lst");
			//エラー処理1-1
			if (! file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			while (line != null) {
				String[] codeName = line.split(",");
				//支店名が空文字列の場合
				if (codeName.length != 2) {
					//エラー処理1-2
					System.out.println("支店定義ファイルのフォーマットが不正です");
					br.close();
					return;
				}
				//支店コードの重複チェック(branchNames.containsKey(cn[0]))は省略
				//支店コードが3桁の数字でない場合
				if(!codeName[0].matches("^\\d{3}")) {
					//エラー処理1-2
					System.out.println("支店定義ファイルのフォーマットが不正です");
					br.close();
					return;
				}
				branchesNames.put(codeName[0], codeName[1]);
				branchesSales.put(codeName[0], 0L);
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			//エラー処理3
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//処理2 集計
		/*
		2-1 売上ファイルが連番になっていない場合は、エラーメッセージ「売上ファイル名が連番になっていません」を表示し、処理を終了する。
		2-2 合計金額が10桁を超えた場合、エラーメッセージ「合計金額が10桁を超えました」を表示し、処理を終了する。
		2-3 支店に該当がなかった場合は、エラーメッセージ「<該当ファイル名>の支店コードが不正です」と表示し、処理を終了する。
		2-4 売上ファイルの中身が3行以上ある場合は、エラーメッセージ「<該当ファイル名>のフォーマットが不正です」と表示し処理を終了する。
		*/
		//指定されたディレクトリから支店別集計ファイル(.rcd)を探してリスト化。
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.matches("^\\d{8}.rcd");
			}
		};
		File[] files = new File(args[0]).listFiles(filter);
		//エラー処理2-1を確認
		int now = 0;
		for (int i = 0; i < files.length; ++i){
			//リストからファイル名を取得。
			String fileNameRcd = files[i].getName();
			String fileName = fileNameRcd.substring(0, fileNameRcd.lastIndexOf('.'));
			try {
				if (i == 0) {
					now = Integer.parseInt(fileName);
				} else {
					int next = Integer.parseInt(fileName);
					if (now + 1 != next) {
						//エラー処理2-1
						System.out.println("売上ファイル名が連番になっていません");
						return;
					} else {
						now = next;
					}
				}
			} catch (NumberFormatException e) {
				//エラー処理3
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}
		
		for (int i = 0; i < files.length; ++i){
			//リストからファイル名を取得。
			String fileNameRcd = files[i].getName();
			try {
				File file = new File(args[0] , fileNameRcd);
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				String line = br.readLine();
				boolean branchCode = branchesSales.containsKey(line);
				if (!branchCode) {
					//エラー処理2-3
					System.out.println(fileNameRcd + "の支店コードが不正です");
					br.close();
					return;
				}
				String saleCode = line;
				line = br.readLine();
				Long saleSum = branchesSales.get(saleCode);
				try {
					saleSum += Long.parseLong(line);
				} catch (NumberFormatException e) {
					//エラー処理3
					System.out.println("予期せぬエラーが発生しました");
					br.close();
					return;
				}
				if (saleSum >= 10000000000L) {
					//エラー処理2-2
					System.out.println("合計金額が10桁を超えました");
					br.close();
					return;
				}
				branchesSales.put(saleCode, saleSum);
				line = br.readLine();
				if (line != null) {
					//エラー処理2-4
					System.out.println(fileNameRcd + "のフォーマットが不正です");
					br.close();
					return;
				}
				br.close();
			} catch (IOException e) {
				//エラー処理3
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		//処理3 集計結果出力
		try {
			File file = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			for (Map.Entry<String,String> branch : branchesNames.entrySet()) {
				String branchCode = branch.getKey();
				String branchName = branch.getValue();
				Long branchSale = branchesSales.get(branchCode);
				pw.println(branchCode + "," + branchName + "," + branchSale);
			 }
			pw.close();
		} catch (IOException e) {
			//エラー処理3
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}